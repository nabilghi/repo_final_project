/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import DocumentPicker from 'react-native-document-picker';

export default class App extends Component {
  async openDocFile(){
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size
      );
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }
  render(){
    return(
      <View style={{flex : 1,}}>
        <TouchableOpacity
        onPress={()=>this.openDocFile()}
        style={{borderRadius: 10, padding: 10, alignItems: "center", marginVertical: 250, backgroundColor: "red", marginHorizontal: 100}}
        >
        <Text> Choose File </Text>
        </TouchableOpacity>
      </View>
    )
  }
}